package com.adamk.stacksearcher.interfaces

import com.adamk.stacksearcher.models.SearchBase

open class ActivityContract{

    interface Presenter{
        fun onSearchRequest(searchData: SearchBase)
    }

    interface View{
        fun onNewSearchData(searchData: SearchBase)
    }

}
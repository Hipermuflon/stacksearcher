package com.adamk.stacksearcher.interfaces

import android.webkit.WebView

interface WebLoader {

    fun onLoadStart(url: String?)
    fun onProgressChanged(view: WebView?, newProgress: Int)
    fun onTimeout()
    fun onWebError(desc: String?, failingUrl: String?)//error: WebResourceError?)
    fun onLoadFinish(url: String?)

}
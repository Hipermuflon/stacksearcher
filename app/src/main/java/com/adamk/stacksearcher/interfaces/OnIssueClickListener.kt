package com.adamk.stacksearcher.interfaces

import com.adamk.stacksearcher.database.IssueRealm

interface OnIssueClickListener {

    fun onIssueClick(issue:IssueRealm)
}
package com.adamk.stacksearcher.interfaces

import com.adamk.stacksearcher.database.IssueRealm
import com.adamk.stacksearcher.models.SearchData

class MainActivityContract : ActivityContract() {

    interface Presenter : ActivityContract.Presenter{
        fun onPagePrev(searchData: SearchData)
        fun onPageNext(searchData: SearchData)
    }

    interface View : ActivityContract.View{
        fun showResults(issues: List<IssueRealm>)
    }

}
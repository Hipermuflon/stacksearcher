package com.adamk.stacksearcher.utils

import java.util.regex.Pattern

class StringFormatter {

    fun format(title:String):String{
        var strOut = title.replace("&quot;","\"").replace("&amp;","&")
        val p = Pattern.compile("&#\\d{1,3};")
        val matcher = p.matcher(strOut)

        while(matcher.find()){

            try{
                val text = matcher.group().replace("&#","").replace(";","")
                val charCode = Integer.parseInt(text)
                val replacement = charCode.toChar().toString()
                strOut = matcher.replaceAll(replacement)
            }
            catch(e:Exception){
                e.printStackTrace()
            }
        }
        return strOut //title.replace("&quot;","\"").replace("&amp;","&").replace("&#39;","'")
    }
}
package com.adamk.stacksearcher.utils

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.AppCompatImageView
import android.util.TypedValue
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition

class ImageUtils {

    private val IMG_SIZE = 40

    fun loadSquareImage(ctx: Context, resId: String?, target: AppCompatImageView) {

        val size = dp2Px(ctx,IMG_SIZE)

        val tar = object : SimpleTarget<Bitmap>(size, size) {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                target.setImageBitmap(resource)
            }
        }

        Glide.with(ctx).asBitmap().load(resId).into<SimpleTarget<Bitmap>>(tar)
    }

    private fun dp2Px(ctx: Context, dp:Int) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp.toFloat(),ctx.resources.displayMetrics).toInt()
}

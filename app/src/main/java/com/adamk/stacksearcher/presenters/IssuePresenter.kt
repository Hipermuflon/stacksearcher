package com.adamk.stacksearcher.presenters

import com.adamk.stacksearcher.activities.IssueActivity
import com.adamk.stacksearcher.interfaces.ActivityContract
import com.adamk.stacksearcher.models.SearchBase

class IssuePresenter(val view: ActivityContract.View,activity: IssueActivity) : ActivityPresenter(activity) {

    override fun onSearchAvailable(searchData: SearchBase) {

        if(searchData.searchTxt.isNotEmpty() && !searchData.isSearchCompleted()){
            view.onNewSearchData(searchData)
        }
    }
}
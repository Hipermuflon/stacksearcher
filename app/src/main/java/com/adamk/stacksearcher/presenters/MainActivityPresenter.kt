package com.adamk.stacksearcher.presenters

import android.util.Log
import com.adamk.stacksearcher.activities.MainActivity
import com.adamk.stacksearcher.api.SOClient
import com.adamk.stacksearcher.database.ResultHandler
import com.adamk.stacksearcher.interfaces.MainActivityContract
import com.adamk.stacksearcher.models.IssueWrapper
import com.adamk.stacksearcher.models.SearchBase
import com.adamk.stacksearcher.models.SearchData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import io.realm.RealmChangeListener
import java.util.concurrent.TimeUnit


class MainActivityPresenter(val view: MainActivityContract.View, override val activity: MainActivity) : ActivityPresenter(activity), MainActivityContract.Presenter, RealmChangeListener<Realm> {

    private val SOAPI = SOClient().getClient().create(com.adamk.stacksearcher.api.StackExchangeAPI::class.java)
    private val resultHandler = ResultHandler(this)
    lateinit var subject: PublishSubject<Int>

    override fun onChange(t: Realm?) {
        val results = resultHandler.getResults()
        view.showResults(results)
    }

    override fun initPresenter() {
        super.initPresenter()
        subject = PublishSubject.create()
        subject.debounce(1, TimeUnit.SECONDS)
            .subscribe(object : Observer<Int> {
                override fun onNext(page: Int) {
                    if(activity.searchData.isSearchTxt()) {
                        (activity.searchData as SearchData).page = page
                        activity.runOnUiThread {
                            view.onNewSearchData(activity.searchData)
                        }
                    }
                }

                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {}
            })
    }

    override fun onSearchAvailable(searchData: SearchBase) {
        onSearchRequest(searchData)
    }

    override fun onSearchRequest(searchData: SearchBase){
        if(searchData.isSearchTxt())
            getIssues(searchData as SearchData)
    }


    override fun onPagePrev(searchData: SearchData) {
        if(searchData.searchTxt.isNotEmpty() && searchData.prevPage())
            getIssues(searchData)
    }

    override fun onPageNext(searchData: SearchData) {
        if(searchData.searchTxt.isNotEmpty() && searchData.nextPage())
            getIssues(searchData)
    }

    private fun getIssues(searchData: SearchData) {

        SOAPI.getIssues(searchData.searchTxt, searchData.page).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<IssueWrapper> {
            override fun onComplete() {
                Log.d("onComplete", "onComplete")

                searchData.completedSearchTxt = searchData.searchTxt
                searchData.saveLastSearchData(activity)
            }

            override fun onSubscribe(d: Disposable) {
                Log.d("onSubscribe", "onSubscribe")
                compositeDisposable.add(d)
            }

            override fun onNext(response: IssueWrapper) {

                if (response.issues != null) {
                    resultHandler.saveIssueData(response.issues, searchData.page)
                }
            }

            override fun onError(e: Throwable) {
                onRequestError(e, searchData)
            }
        })
    }

    private fun onRequestError(e: Throwable, searchData: SearchData) {
        Log.d("onError", e.localizedMessage)

        val results = resultHandler.getResults(searchData.page)
        if (results.isNotEmpty()) {
            view.showResults(results)
        }
        else {  // ustawiamy stronę na ostatnio znalezioną
            searchData.page = resultHandler.currPage
            searchData.saveLastSearchData(activity)
        }
    }
}
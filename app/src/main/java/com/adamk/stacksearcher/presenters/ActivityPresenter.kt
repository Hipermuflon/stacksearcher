package com.adamk.stacksearcher.presenters

import com.adamk.stacksearcher.activities.BaseActivity
import com.adamk.stacksearcher.models.SearchBase
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class ActivityPresenter(open val activity: BaseActivity) {

    val compositeDisposable = CompositeDisposable()
    private var isFirstCheck = true
    var isConnected = false

    init{
        this.initPresenter()
    }

    open fun initPresenter() {

        compositeDisposable.add(ReactiveNetwork.observeInternetConnectivity()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isConnected ->

                this.isConnected = isConnected
                if(isConnected || (isFirstCheck && !activity.isStateSaved)){
                    val searchData = activity.searchData
                    if(!searchData.isSearchCompleted())
                        onSearchAvailable(searchData)

                    isFirstCheck = false
                }
            })
    }

    abstract fun onSearchAvailable(searchData: SearchBase)
}
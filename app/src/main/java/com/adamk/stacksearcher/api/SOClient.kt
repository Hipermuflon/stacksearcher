package com.adamk.stacksearcher.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class SOClient {

    private val REQUEST_TIMEOUT = 30L
    private val BASE_URL = "http://api.stackexchange.com"

    fun getClient(): Retrofit {

        val okHttpClient = initOkHttp()

        return Retrofit.Builder().baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient).build()
    }

    private fun initOkHttp():OkHttpClient{
        val clientBuilder = OkHttpClient.Builder()
            .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

        return clientBuilder.build()
    }
}
package com.adamk.stacksearcher.api

import com.adamk.stacksearcher.models.IssueWrapper
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface StackExchangeAPI {

    @GET("2.2/search")
    fun getIssues(@Query("intitle") intitle:String, @Query("page") page: Int?=1,
                  @Query("order") order:String = "desc", @Query("pagesize") pageSize: Int?=30,
                  @Query("sort") sort: String = "activity",
                  @Query("site") site: String = "stackoverflow"): Observable<IssueWrapper>
}
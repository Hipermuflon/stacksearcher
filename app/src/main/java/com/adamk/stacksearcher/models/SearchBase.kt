package com.adamk.stacksearcher.models

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.adamk.stacksearcher.BR

open class SearchBase(searchTxt: String = "") : BaseObservable() {

    var searchTxt:String = searchTxt
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.searchTxt)
        }


    var completedSearchTxt:String? = ""
    fun isSearchCompleted():Boolean = searchTxt == completedSearchTxt
    fun isSearchTxt() = searchTxt.isNotEmpty()
}
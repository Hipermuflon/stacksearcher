package com.adamk.stacksearcher.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IssueWrapper {

    @SerializedName("items")
    @Expose
    val issues: List<Issue>? = null
}
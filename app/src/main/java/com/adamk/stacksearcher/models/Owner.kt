package com.adamk.stacksearcher.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Owner {

    @SerializedName("user_id")
    @Expose
    val userId: Int = -1

    @SerializedName("display_name")
    @Expose
    val userName: String = ""

    @SerializedName("profile_image")
    @Expose
    val profileImg: String = ""
}
package com.adamk.stacksearcher.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Issue {

    @SerializedName("answer_count")
    @Expose
    val answerCnt: Int = -1

    private val owner: Owner = Owner()
    val link = ""
    val title = ""

    fun getId():Int = owner.userId
    fun getImgUrl():String = owner.profileImg
    fun getUserName():String = owner.userName
}

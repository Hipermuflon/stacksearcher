package com.adamk.stacksearcher.models

import android.content.Context
import android.databinding.Bindable
import com.adamk.stacksearcher.BR

class SearchData(searchTxt: String = "", page: Int = 1) : SearchBase(searchTxt) {

    var page:Int = page
    @Bindable get() = field
    set(value) {
        if(value!=-1) {
            field = value
            notifyPropertyChanged(BR.page)
        }
    }

    fun prevPage():Boolean{
        if(page == 1)
            return false
        else
            page--
        return true
    }

    fun nextPage():Boolean{
       page++
       return true
    }

    fun getLastSearchData(ctx:Context)=SearchStorage(ctx).getLastSearchData()
    fun saveLastSearchData(ctx:Context)=SearchStorage(ctx).saveLastSearchData(this)

    inner class SearchStorage(private val ctx: Context) {

        private val STORAGE_NAME = "SEARCH_TXT_STORAGE"
        private val LAST_SEARCH = "LAST_SEARCH"
        private val PAGE = "PAGE"

        fun getLastSearchData():SearchData{
            val searchData = SearchData()

            searchData.searchTxt = ctx.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
                    .getString(LAST_SEARCH,"")
            searchData.page = ctx.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
                    .getInt(PAGE,0)

            return searchData
        }

        fun saveLastSearchData(searchData:SearchData){

            ctx.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(LAST_SEARCH,searchData.searchTxt)
                .putInt(PAGE,searchData.page)
                .apply()
        }
    }
}
package com.adamk.stacksearcher.activities

import android.os.Handler
import android.support.design.widget.Snackbar
import android.util.Log
import android.webkit.WebView
import com.adamk.stacksearcher.R
import com.adamk.stacksearcher.interfaces.ActivityContract
import com.adamk.stacksearcher.interfaces.WebLoader
import com.adamk.stacksearcher.models.SearchBase
import com.adamk.stacksearcher.models.SearchData
import com.adamk.stacksearcher.presenters.IssuePresenter
import kotlinx.android.synthetic.main.issue_detail_view.*

class IssueActivity : BaseActivity(), ActivityContract.View, WebLoader {

    private val handler = Handler()

    override fun executeBindings() {
        setContentView(R.layout.issue_detail_view)
        activityPresenter = IssuePresenter(this,this)
    }

    override fun initListeners(){

        searchData = SearchData(intent.getStringExtra(BaseActivity.ISSUE_LINK))

        if(activityPresenter.isConnected)
            activityPresenter.onSearchAvailable(searchData)

        webView.webLoader = this
        webView.viewTreeObserver.addOnScrollChangedListener { swipeRefresh.isEnabled = webView.scrollY == 0}
    }

    override fun onNewSearchData(searchData: SearchBase) {webView.loadUrl(searchData.searchTxt)}

    override fun onLoadStart(url: String?) {handler.post({progressBar.startLoading()})}

    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        handler.post({
            progressBar.setProgress(newProgress,true)
        })
    }

    override fun onTimeout() {
        finishWithErrorMessage(getString(R.string.timeout_msg))
    }

    override fun onWebError(desc: String?, failingUrl: String?){//error: WebResourceError?) {
        if(desc.isNullOrEmpty())
            finishWithErrorMessage(getString(R.string.download_error_msg))
        else
            finishWithErrorMessage(getString(R.string.download_error_msg) + ": $desc")
    }

    override fun onLoadFinish(url: String?) {
        Log.d("onLoadFinish","onLoadFinish")
        swipeRefresh.isRefreshing = false
        handler.post({progressBar.finishLoading()})
    }

    private fun finishWithErrorMessage(msg:String) {
        swipeRefresh.isRefreshing = false
        val snack = Snackbar.make(swipeRefresh, msg, Snackbar.LENGTH_SHORT)
        snack.setActionTextColor(resources.getColor(R.color.white))
        snack.setAction(getString(R.string.retry)) {
            onNewSearchData(searchData)
        }
        snack.show()
        snack.view.setBackgroundColor(resources.getColor(R.color.colorAccent))
        handler.post({ progressBar.finishLoading() })
    }
}
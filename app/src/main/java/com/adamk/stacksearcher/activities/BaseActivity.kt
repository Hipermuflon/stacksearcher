package com.adamk.stacksearcher.activities

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import com.adamk.stacksearcher.R
import com.adamk.stacksearcher.models.SearchBase
import com.adamk.stacksearcher.presenters.ActivityPresenter
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity : AppCompatActivity(){

    protected lateinit var activityPresenter: ActivityPresenter
    lateinit var searchData: SearchBase
    var isStateSaved: Boolean = false


    companion object {
        const val ISSUE_LINK = "ISSUE_LINK"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isStateSaved = savedInstanceState!=null
        executeBindings()

        setSupportActionBar(toolbar)

        val swipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)

        swipeRefreshLayout.setOnRefreshListener {
            if(activityPresenter.isConnected){
                activityPresenter.onSearchAvailable(searchData)
            }
            else
                swipeRefreshLayout.isRefreshing = false
        }

        initListeners()
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        isStateSaved = true
    }

    abstract fun executeBindings()
    abstract fun initListeners()

    public override fun onDestroy() {
        activityPresenter.compositeDisposable.clear()
        super.onDestroy()
    }
}
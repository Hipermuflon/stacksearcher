package com.adamk.stacksearcher.activities

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.support.v7.widget.SearchView.SearchAutoComplete
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import com.adamk.stacksearcher.BR
import com.adamk.stacksearcher.R
import com.adamk.stacksearcher.adapters.IssuesAdapter
import com.adamk.stacksearcher.database.IssueRealm
import com.adamk.stacksearcher.databinding.ActivityMainBinding
import com.adamk.stacksearcher.interfaces.MainActivityContract
import com.adamk.stacksearcher.interfaces.OnIssueClickListener
import com.adamk.stacksearcher.models.SearchBase
import com.adamk.stacksearcher.models.SearchData
import com.adamk.stacksearcher.presenters.MainActivityPresenter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), SearchView.OnQueryTextListener, View.OnTouchListener, MainActivityContract.View, OnIssueClickListener {

    private lateinit var searchAC: SearchAutoComplete

    override fun executeBindings() {
        val binding:ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityPresenter = MainActivityPresenter(this,this)
        searchData = SearchData().getLastSearchData(this)
        binding.setVariable(BR.searchData,searchData)
        binding.setVariable(BR.presenter,activityPresenter)
    }

    override fun initListeners() {
        initList()
        issuesList.setOnTouchListener(this)

        pageET.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                (activityPresenter as MainActivityPresenter).subject.onNext(s.toIntOrMinusOne())
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        val searchItem = menu?.findItem(R.id.search)

        val mSearchView = MenuItemCompat.getActionView(searchItem) as SearchView
        mSearchView.setOnQueryTextListener(this)
        mSearchView.setIconifiedByDefault(true)
        mSearchView.isIconified = false
        searchAC = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as SearchAutoComplete
        searchAC.setBackgroundResource(R.drawable.et_bkg)
        searchAC.setTextColor(resources.getColor(R.color.black))
        searchAC.setHintTextColor(resources.getColor(R.color.black))

        val searchField = SearchView::class.java.getDeclaredField("mCloseButton")
        searchField.isAccessible = true
        val mSearchCloseButton = searchField.get(mSearchView) as ImageView
        mSearchCloseButton.setImageDrawable(ColorDrawable(Color.TRANSPARENT))//R.drawable.ic_close)
        mSearchCloseButton.isEnabled = false

        if(searchData.searchTxt.isNotEmpty())
            onSearchRequest()

        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return onQueryTextChange(query)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            searchData.searchTxt = newText
            (searchData as SearchData).page = 1
            onSearchRequest()
        }
        return true
    }

    private fun onSearchRequest() {
        (activityPresenter as MainActivityPresenter).onSearchRequest(searchData)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if(searchAC.hasFocus()) {
            searchAC.clearFocus()
            hideSoftKeyboard()
        }
    }

    override fun onTouch(v: View, e: MotionEvent): Boolean {
        hideSoftKeyboard(v)
        return false
    }

    override fun onResume() {
        super.onResume()
        hideSoftKeyboard(issuesList)
    }

    private fun initList() {

        val linearLayoutManager = LinearLayoutManager(this)
        issuesList.layoutManager = linearLayoutManager
        issuesList.setHasFixedSize(true)
        issuesList.adapter = IssuesAdapter(this)
    }

    private fun hideSoftKeyboard(v: View = searchAC) {

        if (v.windowToken == null)
            return

        val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
    }

    override fun onNewSearchData(searchData: SearchBase) {
        if(searchData.searchTxt.isNotEmpty()) {

            this.searchData = searchData as SearchData

            if(searchAC.setTextIfChanged(searchData.searchTxt))
                    searchAC.setSelection(searchData.searchTxt.length)

            onSearchRequest()
        }
    }

    override fun showResults(issues: List<IssueRealm>) {
        swipeRefresh.isRefreshing = false
        issuesList.adapter = IssuesAdapter(this@MainActivity, issues)
    }

    override fun onIssueClick(issue: IssueRealm) {

        hideSoftKeyboard()
        val intent = Intent(this@MainActivity, IssueActivity::class.java)
        intent.putExtra(ISSUE_LINK,issue.link)
        startActivity(intent)
    }
}

private fun EditText.setTextIfChanged(txt: String):Boolean {
    return if(this.text.toString() != txt) {
        this.setText(txt)
        true
    } else false
}

private fun CharSequence?.toIntOrMinusOne(): Int = toString().toIntOrNull() ?: -1
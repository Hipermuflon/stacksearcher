package com.adamk.stacksearcher.app

import android.app.Application
import com.adamk.stacksearcher.utils.TypefaceUtil
import io.realm.Realm
import io.realm.RealmConfiguration

class StackSearcher : Application() {

    override fun onCreate() {
        super.onCreate()
        TypefaceUtil.overrideFont(applicationContext, "SERIF", "fonts/Ubuntu-R.ttf")
        initRealm()
    }


    private fun initRealm() {
        Realm.init(this)

        val realmConf = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(realmConf)
    }
}
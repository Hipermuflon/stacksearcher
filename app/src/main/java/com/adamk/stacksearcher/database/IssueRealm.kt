package com.adamk.stacksearcher.database

import com.adamk.stacksearcher.models.Issue
import com.adamk.stacksearcher.utils.StringFormatter
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class IssueRealm  : RealmObject {

    @PrimaryKey
    var id: Int = -1
    var answerCnt: Int = -1
    var link = ""
    var imgUrl = ""
    private var pageNum: Int = 1
    var userName = ""
    var title = ""

    constructor()

    constructor(issue: Issue, page:Int=1){
        id = issue.getId()
        answerCnt = issue.answerCnt
        userName = StringFormatter().format(issue.getUserName())
        link = issue.link
        imgUrl = issue.getImgUrl()
        pageNum = page
        title = StringFormatter().format(issue.title)
    }
}
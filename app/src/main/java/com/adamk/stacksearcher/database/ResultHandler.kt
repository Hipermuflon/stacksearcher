package com.adamk.stacksearcher.database

import com.adamk.stacksearcher.models.Issue
import io.realm.Realm
import io.realm.RealmChangeListener
import java.util.*

class ResultHandler(changeListener: RealmChangeListener<Realm>? = null) {

    var currPage = 1

    fun getResults(page:Int=currPage): List<IssueRealm>{

        val data = Realm.getDefaultInstance().where(IssueRealm::class.java).equalTo("pageNum",page).findAll()
        val result = ArrayList<IssueRealm>()
        result.addAll(data)

        return result
    }

    fun saveIssueData(repos: List<Issue>, page:Int) {

        val realm = Realm.getDefaultInstance()

        try {
            realm.executeTransaction { rlm ->
                rlm.deleteAll()
                for (repo in repos) {
                    currPage = page
                    rlm.copyToRealmOrUpdate(IssueRealm(repo,page))
                }
            }
        }
        catch (e: Exception) {
            if(realm.isInTransaction)
                realm.cancelTransaction()
        }
    }

    init {
        if(changeListener!=null)
            Realm.getDefaultInstance().addChangeListener(changeListener)
    }
}
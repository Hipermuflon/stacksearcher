package com.adamk.stacksearcher.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adamk.stacksearcher.R
import com.adamk.stacksearcher.activities.MainActivity
import com.adamk.stacksearcher.database.IssueRealm
import com.adamk.stacksearcher.utils.ImageUtils
import kotlinx.android.synthetic.main.item_view.view.*

class IssuesAdapter(val mainActivity: MainActivity, private var items: List<IssueRealm> = listOf()) :RecyclerView.Adapter<IssuesAdapter.ViewHolder>() {

    private val VIEW_TYPE_EMPTY = -1
    private val imageUtils = ImageUtils()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType){
            VIEW_TYPE_EMPTY -> ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.empty_view, parent, false))
            else            -> ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(items.size){
            0    -> VIEW_TYPE_EMPTY
            else -> position
        }
    }

    override fun getItemCount(): Int {

        return when(items.size){
            0    -> 1
            else -> items.size}
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(items.isNotEmpty()) {

            val issue = items[position]

            holder.itemView.setOnClickListener {
                mainActivity.onIssueClick(issue)
            }

            holder.putData(issue)
        }
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun putData(issue: IssueRealm) {
            itemView.titleTV.text = issue.title
            itemView.answersTV.text = issue.answerCnt.toString()
            itemView.userTV.text = issue.userName
            itemView.setBackgroundResource(R.drawable.item_selector, true)
            imageUtils.loadSquareImage(mainActivity,issue.imgUrl,itemView.avatarImg)

        }
    }
}

private fun View.setBackgroundResource(resId: Int, isKeepPaddings: Boolean) {
    val paddings: Array<Int> = arrayOf(this.paddingLeft,this.paddingTop,this.paddingRight,this.paddingBottom)
    this.setBackgroundResource(resId)

    if(isKeepPaddings)
        this.setPadding(paddings[0],paddings[1],paddings[2],paddings[3])
}

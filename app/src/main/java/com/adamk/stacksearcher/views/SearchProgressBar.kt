package com.adamk.stacksearcher.views

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ProgressBar

class SearchProgressBar(context: Context?, attrs: AttributeSet?) : ProgressBar(context, attrs) {

    private val alphaIn = AlphaAnimation(0f,1f)
    private val alphaOut = AlphaAnimation(1f,0f)

    init{
        alphaIn.duration = 400L
        alphaOut.duration = 800L
    }

    fun startLoading(){setProgress(0, true)}
    fun finishLoading(){setProgress(100, true)}

    override fun setProgress(progress: Int,animate:Boolean) {

        if(progress == 100)
            visibility = View.GONE
        if(progress==0)
            visibility = View.VISIBLE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            super.setProgress(progress, animate)
        else
            super.setProgress(progress)
    }

    override fun setVisibility(visibility: Int) {

        if (getVisibility() == visibility)
            return

        if (visibility == View.VISIBLE) {
            startAnimation(alphaIn)
            super.setVisibility(visibility)
        }
        else{
            startAnimation(alphaOut)
            alphaOut.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {}

                override fun onAnimationEnd(p0: Animation?) {
                    super@SearchProgressBar.setVisibility(visibility)
                }

                override fun onAnimationStart(p0: Animation?) {}

            })
        }
    }
}
package com.adamk.stacksearcher.views

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.adamk.stacksearcher.R

class AnimationImgView(context: Context?, attrs: AttributeSet?) : android.support.v7.widget.AppCompatImageView(context, attrs) {

    private val inAnimation: Animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)

    init{
        inAnimation.fillBefore = true
        inAnimation.fillAfter = true
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        startAnimation(inAnimation)
    }
}
package com.adamk.stacksearcher.views

import android.content.Context
import android.graphics.Bitmap
import android.os.CountDownTimer
import android.util.AttributeSet
import android.util.Log
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.adamk.stacksearcher.R
import com.adamk.stacksearcher.interfaces.WebLoader


class Browser(context: Context?, attrs: AttributeSet?) : WebView(context, attrs) {

    var webLoader: WebLoader? = null
    private var isError = false
    private val TIMEOUT = 15000L

    val timer : CountDownTimer

    init{

        timer = object: CountDownTimer(TIMEOUT,TIMEOUT){
            override fun onTick(p0: Long) {}

            override fun onFinish() {
                stopLoading()
                isError = true
                webLoader!!.onTimeout()
            }
        }

        initSettings()
        initClient()
    }

    private fun initSettings() {
        settings.javaScriptEnabled = true
    }

    private fun initClient(){

        webViewClient = object: WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return false
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                timer.start()
                isError = false
                Log.d("browser","onPageStarted: $url")
                webLoader!!.onLoadStart(url)
            }
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                timer.cancel()
                Log.d("browser","onFinished: $url")

                if (!isError) {
                    webLoader!!.onLoadFinish(url)//originalUrl)
                }

            }

            override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                super.onReceivedError(view, errorCode, description, failingUrl)
                isError = true
                timer.cancel()
                Log.d("browser","onReceivedError: $description for url $failingUrl")
                webLoader!!.onWebError(getMappedDescription(description),failingUrl)
            }
        }
        webChromeClient = object: WebChromeClient() {

            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                Log.d("browser","onProgressChanged: $newProgress")
                if(newProgress<100)
                    webLoader!!.onProgressChanged(view, newProgress)
            }
        }
    }
    private fun getMappedDescription(description: String?):String?{
        return when {
            description.isNullOrEmpty() -> description
            description!!.contains("NAME_NOT_RESOLVED") -> context.getString(R.string.no_addr_found)
            description.contains("INTERNET_DISCONNECTED") -> context.getString(R.string.no_internet_msg)
            else -> description
        }
    }
}

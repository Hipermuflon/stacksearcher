package com.adamk.stacksearcher

import com.adamk.stacksearcher.utils.StringFormatter
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun testStringFormat(){

        testFormatter("test&#39;TEST&#39;test","test'TEST'test")
        testFormatter("Removing &#39;TER&#39; keywords from PDB","Removing 'TER' keywords from PDB")
        testFormatter("ADB server version (36) doesn&#39;t match","ADB server version (36) doesn't match")
        testFormatter("&amp;test&#39;&amp;TEST&#39;test","&test'&TEST'test")
    }


    private fun testFormatter(testStr:String,expectedStr:String){
        val actualStr = StringFormatter().format(testStr)
        assertEquals("Formatterfailed",expectedStr,actualStr)
    }
}

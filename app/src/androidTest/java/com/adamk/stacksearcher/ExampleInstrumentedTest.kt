package com.adamk.stacksearcher

import android.support.test.InstrumentationRegistry
import android.support.test.filters.MediumTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.widget.ListView
import com.adamk.stacksearcher.activities.MainActivity
import com.adamk.stacksearcher.adapters.IssuesAdapter
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.adamk.stacksearcher", appContext.packageName)
    }
}

@MediumTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    var rule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    @Throws(Exception::class)
    fun ensureListViewIsPresent() {
        val activity = rule.activity
        val viewById = activity.findViewById<RecyclerView> (R.id.issuesList)
        assertThat(viewById, notNullValue())
        assertThat(viewById, instanceOf(ListView::class.java))
        val recyclerView = viewById as RecyclerView
        val adapter = recyclerView.adapter
        assertThat(adapter, instanceOf(IssuesAdapter::class.java))
    }
}
